//
//  User.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 15/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import Foundation
struct User: Decodable {
    var userName: String?
    var greeting: String?
    var emailAddress : String?
    var firstName : String?
    var lastName : String?
    var lastLoginDate : Int?
    var jobTitle : String?
    var createDate : Int?
    var lastLoginIP : String?
    
    init(userName: String? = nil,
         greeting: String? = nil,
         emailAddress : String? = nil,
         lastName : String? = nil,
         firstName : String? = nil,
         lastLoginDate : Int? = nil,
         jobTitle : String? = nil,
         createDate : Int? = nil,
         lastLoginIP : String? = nil)
    
    {
        self.userName = userName
        self.greeting = greeting
        self.emailAddress = emailAddress
        self.firstName = firstName
        self.lastName = lastName
        self.jobTitle = jobTitle
        self.createDate = createDate
        self.lastLoginIP = lastLoginIP
    }
    
}

