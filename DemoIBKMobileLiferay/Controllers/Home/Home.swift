//
//  Home.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 8/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit

class Home: UIViewController {
    
    
    @IBOutlet weak var ViewContainerHomeDetalle: UIView!
    @IBOutlet weak var VireEjemplo: UIView!
    @IBOutlet weak var btnIniciarProceso: UIButton!
    @IBOutlet weak var imgLogoPacifico: UIImageView!
    var Configure = Singleton.shared
    override func viewDidLoad() {
        
        super.viewDidLoad()
            self.VireEjemplo.backgroundColor = UIColor(hexString:self.Configure.NackGroundColors!)
            self.btnIniciarProceso.backgroundColor = UIColor(hexString:self.Configure.NackGroundColors!)
            self.btnIniciarProceso.Extensioncornerbutton()
    }


}
