//
//  Charts.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 11/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit


class Charts: UIViewController {
    @IBOutlet var viewContainerGeneral: UIView!
//    @IBOutlet weak var viewCharts: PieChartView!
    @IBOutlet weak var btnComprarPromocion: UIButton!
    
    var Configure = Singleton.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.viewCharts.backgroundColor = UIColor.white
        self.viewContainerGeneral.backgroundColor = UIColor(hexString:self.Configure.NackGroundColors!)
//        self.viewCharts.chartDescription?.text = ""
//        self.updateChartDataaa()
        self.btnComprarPromocion.layer.cornerRadius = 5
        self.btnComprarPromocion.layer.borderWidth = 2
        self.btnComprarPromocion.layer.borderColor = UIColor(hexString:self.Configure.NackGroundColors!).cgColor
        self.btnComprarPromocion.setTitleColor(UIColor(hexString:self.Configure.NackGroundColors!), for: .normal)
    }
    
//    func updateChartDataaa()  {
//        
//        // 2. generate chart data entries
//        let track = ["Income", "Expense", "Wallet", "Bank"]
//        let money = [650, 456.13, 78.67, 856.52]
//        
//        var entries = [PieChartDataEntry]()
//        for (index, value) in money.enumerated() {
//            let entry = PieChartDataEntry()
//            entry.y = value
//            entry.label = track[index]
//            entries.append( entry)
//        }
//        
//        // 3. chart setup
//        let set = PieChartDataSet( values: entries, label: "")
//        // this is custom extension method. Download the code for more details.
//        var colors: [UIColor] = []
//        
//        for _ in 0..<money.count {
//            let red = Double(arc4random_uniform(256))
//            let green = Double(arc4random_uniform(256))
//            let blue = Double(arc4random_uniform(256))
//            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
//            colors.append(color)
//        }
//        set.colors = colors
//        let data = PieChartData(dataSet: set)
//        self.viewCharts.data = data
//        self.viewCharts.noDataText = "No data available"
//        // user interaction
//        self.viewCharts.isUserInteractionEnabled = true
//        
//        let d = Description()
//        d.text = "DEMO-IBK"
//        self.viewCharts.chartDescription = d
//        self.viewCharts.centerText = "IBK"
//        self.viewCharts.holeRadiusPercent = 0.2
//        self.viewCharts.transparentCircleColor = UIColor.clear
//        
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
