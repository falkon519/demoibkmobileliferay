//
//  Perfil.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 7/03/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit
import LiferayScreens
class Perfil: UIViewController,UserPortraitScreenletDelegate {
    
    @IBOutlet weak var imgPropaganda: UIImageView!
    @IBOutlet weak var lblfullName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lbljobTitle: UILabel!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblLastLoginIp: UILabel!
    @IBOutlet weak var containerViewPerfil: UIView!
    @IBOutlet weak var viewContainerPhoto: UIView!
    @IBOutlet weak var userScreenLet: UserPortraitScreenlet!{
        didSet {
            userScreenLet?.presentingViewController = self
        }
    }
    
    var Configure = Singleton.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        self.getUserData()
        self.userScreenLet.delegate = self
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        print("ENTRO A PERFILLL")
        self.imgPropaganda.downloaded(from: "http://192.168.173.1:8081/documents/34603/0/PromocionIBK.jpg/f69d840a-cec8-98fa-c2cb-1443ca86a888?t=1551382468566")
        self.userScreenLet.loadLoggedUserPortrait()
    }
    
    func initView(){

        self.userScreenLet.autoLoad = true
        self.containerViewPerfil.backgroundColor = UIColor(hexString: Configure.NackGroundColors!)
        
    }
    
    func getUserData (){
        CoreDataUtils.sharedInstance.retrieveAllData(){ (personaCore, error) in
            if(personaCore != nil){                
                self.lblfullName.text = (personaCore!.firstName!) + " " + (personaCore!.lastName!)
                self.lblUserName.text = (personaCore!.userName!)
                self.lblEmail.text = personaCore?.emailAddress
                self.lbljobTitle.text = personaCore?.jobTitle
                self.lblCreateDate.text = String(personaCore!.createDate!)
                self.lblLastLoginIp.text = personaCore!.lastLoginIP!
                
            }
        }
    }
    
    @IBAction func onPressLoout(_ sender: Any) {
        SessionContext.currentContext?.removeStoredCredentials()
        SessionContext.logout()
        CoreDataUtils.sharedInstance.deleteAllData()
        self.performSegue(withIdentifier: "showLogin", sender: self)
        
        print("Cerrar Sesion")
    }
    
    func screenlet(_ screenlet: UserPortraitScreenlet, onUserPortraitResponseImage image: UIImage) -> UIImage {
        print("IMAGENNN --->", image)
        return image
    }
    func screenlet(_ screenlet: UserPortraitScreenlet, onUserPortraitError error: NSError) {
        print("PERFIL ERROR --->", error)
    }
}
