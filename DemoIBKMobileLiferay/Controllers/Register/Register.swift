//
//  Register.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 8/03/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit
import LiferayScreens
class Register: UIViewController,SignUpScreenletDelegate{
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet weak var viewContainerHead: UIView!
    @IBOutlet weak var viewRegisterScreenlet: SignUpScreenlet!
    
    var Configure = Singleton.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewRegisterScreenlet.delegate = self as! BaseScreenletDelegate
        self.initView()
        
    }
    func initView(){   
        self.viewContainerHead.backgroundColor = UIColor(hexString: Configure.NackGroundColors!)
    }
    
    func screenlet(_ screenlet: SignUpScreenlet, onSignUpResponseUserAttributes attributes: [String : AnyObject]) {
        print("attributes --->", attributes)
        
        do {
            print("ENTRAAA")
            let jsonData = try JSONSerialization.data(withJSONObject: attributes, options: .prettyPrinted)
            print("jsonData",jsonData)
            var user = User()
            user = try! JSONDecoder().decode(User.self, from: jsonData)
            CoreDataUtils.sharedInstance.createData(userData: user)
            self.performSegueWithActivity(segue: "showHome");
            self.viewRegisterScreenlet.saveCredentials = true
            print("user --->", user)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func screenlet(_ screenlet: SignUpScreenlet, onSignUpError error: NSError) {
        print("error --->", error)
    }
    
    func performSegueWithActivity(segue:String) {
        // self.hideActivityIndicator()
        self.performSegue(withIdentifier: segue, sender: self)
    }
    
    //funcoin Opcional Para pasar datos por viewcontroller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Entra al segue")
        print("SEGUEEE ", segue.destination)
        //        if let destinationVC = segue.destination as? Home {
        //            print("testeando emailafrees111111 --->", self.emailAddress)
        //            print("testeando lastname ----->", self.lastName)
        //            print("testeando firstName ----->", self.firstName)
        //            print("testeando lastLoginDate----->", self.lastLoginDate)
        //            print("testeando jobTitle----->", self.jobTitle)
        //            print("testeando createDate----->", self.createDate)
        //            destinationVC.emailAddress = self.emailAddress
        //            destinationVC.lastName = self.lastName
        //            destinationVC.firstName = self.firstName
        //            destinationVC.lastLoginDate = self.lastLoginDate
        //            destinationVC.jobTitle = self.jobTitle
        //            destinationVC.createDate = self.createDate
        //        }
    }
    
    @IBAction func onBackButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        print ("back")
    }
    

}
