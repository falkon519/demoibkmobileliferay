//
//  Saldos.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 11/03/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit
import LiferayScreens
class Saldos: UIViewController, UITableViewDelegate,UITableViewDataSource, WebContentDisplayScreenletDelegate {

    @IBOutlet weak var sccreenLetWebContent: WebContentDisplayScreenlet!
    @IBOutlet weak var tableViewSaldos: UITableView!
    
    let saldos = ["10.20","7850.20","12000.32","4797.00","1200.00"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewSaldos.delegate = self
        self.tableViewSaldos.dataSource = self
        self.sccreenLetWebContent.clipsToBounds = true
        self.sccreenLetWebContent.extensionCornerRadius(border: 10)
        SessionContext.loadStoredCredentials()
        self.sccreenLetWebContent.delegate = self
//        self.sccreenLetWebContent.loadWebContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.sccreenLetWebContent.loadWebContent()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.saldos.count
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 90
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "saldosCell") as! SaldosCell
            cell.lblMontoSaldo.text = self.saldos[indexPath.row]
        return cell
    }
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onWebContentError error: NSError) {
        print("onWebContentError --->", error)
    }
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onRecordContentResponse record: DDLRecord) {
        print("onRecordContentResponse ---->", record)
    }
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onUrlClicked url: String) -> Bool {
        print("onUrlClicked --->", url)
        return true
    }
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onWebContentResponse html: String) -> String? {
        print("WebContentDisplayScreenlet ---->",html)
        return html
    }
    
}
