//
//  SaldosCell.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 11/03/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit

class SaldosCell: UITableViewCell {
    @IBOutlet weak var viewContainerSaldos: UIView!
    @IBOutlet weak var lblMontoSaldo: UILabel!
    @IBOutlet weak var textNombreCuenta: UILabel!
    @IBOutlet weak var txtdescripcion: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initView()
    }
    
    func initView(){
        self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
