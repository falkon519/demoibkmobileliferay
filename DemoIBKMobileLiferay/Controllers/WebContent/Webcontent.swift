//
//  Webcontent.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 14/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit
import LiferayScreens

class Webcontent: UIViewController,WebContentDisplayScreenletDelegate {

    var articleId: String?
    
    @IBOutlet weak var screenlet: WebContentDisplayScreenlet!{
        didSet {
            screenlet.delegate = self
            screenlet.articleId = self.articleId ??
                LiferayServerContext.stringPropertyForKey("webContentDisplayArticleId")
            screenlet.customCssFile = "custom"
        }
    }
    
        override func viewDidLoad() {
        super.viewDidLoad()
//        self.testcontentView.delegate = self
        SessionContext.loginWithBasic(username: "test@liferay.com", password: "test", userAttributes: [:] );
        self.initView()
    }
    func initView(){
        print("articloId", self.articleId ?? "no hay article ID")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        articleId = nil
    }
    
//    func screenlet(_ screenlet: WebContentDisplayScreenlet, onWebContentError error: NSError) {
//        print("Error", error)
//    }
//
//    func screenlet(_ screenlet: WebContentDisplayScreenlet, onUrlClicked url: String) -> Bool {
//        print("URL", url)
//        return true
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
