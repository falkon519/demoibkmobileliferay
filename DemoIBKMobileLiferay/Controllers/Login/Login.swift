//
//  Login.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 9/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit
import LiferayScreens
import Photos
import UserNotifications

class Login: UIViewController,LoginScreenletDelegate, WebContentDisplayScreenletDelegate {

    @IBOutlet weak var viewContainerRecuperarRegistrar: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewContainerlogin: LoginScreenlet!
    @IBOutlet weak var viewLoginScreenLet: LoginScreenlet!
    
    var Configure = Singleton.shared
    let photos = PHPhotoLibrary.authorizationStatus()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoginScreenLet.delegate = self
        self.initView()
    }
    
    func initView(){
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        self.viewLoginScreenLet.viewModel.userName = "test@liferay.com"
        self.viewLoginScreenLet.viewModel.password = "test"
        self.viewContainerRecuperarRegistrar.backgroundColor =  UIColor(white: 1, alpha: 0.8)
        self.viewContainerRecuperarRegistrar.extensionCornerRadius(border: 2)
//        self.viewContainer.backgroundColor = UIColor(hexString: self.Configure.NackGroundColors!)
//        self.viewContainerlogin.backgroundColor = UIColor(hexString: self.Configure.NackGroundColors!)
    }
    
    func screenlet(_ screenlet: BaseScreenlet, onLoginResponseUserAttributes attributes: [String : AnyObject]) {
        
        print("LOGIN", attributes)
        
        do {
            print("ENTRAAA")
            let jsonData = try JSONSerialization.data(withJSONObject: attributes, options: .prettyPrinted)
            print("jsonData",jsonData)
            var user = User()
            user = try! JSONDecoder().decode(User.self, from: jsonData)
            CoreDataUtils.sharedInstance.createData(userData: user)
            self.performSegueWithActivity(segue: "showHome");
            self.viewLoginScreenLet.saveCredentials = true
            print("user --->", user)
        } catch {
            print("ERROR",error.localizedDescription)
        }
    }
    
    func screenlet(_ screenlet: BaseScreenlet, onLoginError error: NSError) {
        print("ERROR --->", error)
    }
    
    func performSegueWithActivity(segue:String) {
        // self.hideActivityIndicator()
        self.performSegue(withIdentifier: segue, sender: self)
    }
    
    
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onWebContentResponse html: String) -> String? {
        print("WebContentDisplayScreenlet ---->",html)
        return "Hello"
    }
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onWebContentError error: NSError) {
        print("WebContentDisplayScreenlet Error --->", error)
    }
    
    //funcoin Opcional Para pasar datos por viewcontroller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Entra al segue")
        print("SEGUEEE ", segue.destination)
//        if let destinationVC = segue.destination as? Home {
//            print("testeando emailafrees111111 --->", self.emailAddress)
//            print("testeando lastname ----->", self.lastName)
//            print("testeando firstName ----->", self.firstName)
//            print("testeando lastLoginDate----->", self.lastLoginDate)
//            print("testeando jobTitle----->", self.jobTitle)
//            print("testeando createDate----->", self.createDate)
//            destinationVC.emailAddress = self.emailAddress
//            destinationVC.lastName = self.lastName
//            destinationVC.firstName = self.firstName
//            destinationVC.lastLoginDate = self.lastLoginDate
//            destinationVC.jobTitle = self.jobTitle
//            destinationVC.createDate = self.createDate
//        }
    }
    @IBAction func onPressRegister(_ sender: Any) {
        performSegue(withIdentifier: "showRegister", sender: self)
    }
    
    @IBAction func onPressRecuperarContraseña(_ sender: Any) {
        performSegue(withIdentifier: "showRecuperarContraseña", sender: self)
    }
    
    
}
