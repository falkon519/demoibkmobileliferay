//
//  LaunchScreen.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 8/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit
import LiferayScreens
import Photos
import UserNotifications
class Splah: UIViewController {
    @IBOutlet weak var ViewContainerSplah: UIView!
    @IBOutlet weak var PrincipalImageSplash: UIImageView!
    @IBOutlet var SuperView: UIView!
    var segue = ""
    var backGroundColorContainer = ""
    var Configure = Singleton.shared
    override func viewDidLoad() {
//        self.configureColor()
        super.viewDidLoad()
        self.initView()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.isLogin()
    }
    
    func initView(){
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        self.SuperView.backgroundColor = UIColor(hexString:self.Configure.NackGroundColors!)
        self.ViewContainerSplah.backgroundColor = UIColor(hexString:self.Configure.NackGroundColors!)
        self.PrincipalImageSplash.image = UIImage(named: "home_imglogointerbank")
        self.segue = "showLogin"
//        self.isLogin()
    }
    
    func LoadController() {
        self.perform(#selector(showSplash), with: nil, afterDelay: 0)
    }
    func LoadControllerHome() {
        self.perform(#selector(showHome), with: nil, afterDelay: 0)
    }
    
    @objc func showSplash() -> Void {
        self.performSegue(withIdentifier: self.segue, sender: self)
    }
    @objc func showHome() -> Void {
        self.performSegue(withIdentifier: "showHome", sender: self)
    }
    func isLogin(){
        CoreDataUtils.sharedInstance.retrieveAllData(){ (personaCore, error) in
            if (personaCore != nil){
                print("PersonaCore- --->", personaCore)
            }
    }
        print("Esta Logeado ?",SessionContext.loadStoredCredentials())
        if(SessionContext.loadStoredCredentials()){
            self.LoadControllerHome()
        }else{
            self.LoadController()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
