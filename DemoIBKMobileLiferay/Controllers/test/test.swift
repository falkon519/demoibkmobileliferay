//
//  test.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 14/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit
import LiferayScreens

class test: UIViewController, WebContentDisplayScreenletDelegate {
    
    
    @IBOutlet weak var viewContainierHead: UIView!
    @IBOutlet weak var testwebcontentScreen: WebContentDisplayScreenlet!
    
    let Configure = Singleton.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(hexString:self.Configure.NackGroundColors!)
        self.viewContainierHead.backgroundColor = UIColor(hexString: Configure.NackGroundColors!)
        SessionContext.loadStoredCredentials()
        print("Esta logeado ? ---> test", SessionContext.isLoggedIn)
        self.testwebcontentScreen.delegate = self
        self.testwebcontentScreen.loadWebContent()
    }
    
    
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onWebContentResponse html: String) -> String? {
        print("onWebContent-response --->", html)
        return html
    }
    
    func screenlet(_ screenlet: WebContentDisplayScreenlet, onWebContentError error: NSError) {
        print("Error --> WebContent", error)
    }

}
