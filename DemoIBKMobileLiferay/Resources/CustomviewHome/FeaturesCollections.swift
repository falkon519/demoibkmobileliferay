//
//  FeaturesCollections.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 9/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit

class FeaturesCollections: UICollectionView,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
        var Promociones = ["1", "2"]
    
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self        
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return  Promociones.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Product0Cell", for: indexPath) as! Product0Cell
        cell.imgPropaganda.image = UIImage(named: self.Promociones[indexPath.item])
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var medidas : CGSize = CGSize(width:360, height: 250)
        return medidas
    }
    
}
