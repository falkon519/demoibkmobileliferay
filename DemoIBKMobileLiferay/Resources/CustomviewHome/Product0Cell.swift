//
//  Product0Cell.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 9/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import UIKit

class Product0Cell: UICollectionViewCell {
    
    @IBOutlet weak var viewContainerPropaganda: UIView!
    @IBOutlet weak var imgPropaganda: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    func initView(){
        self.viewContainerPropaganda.extensionCornerRadius(border: 10)                                     
    }
}
