//
//  DefaultColorSingleton.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 8/02/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//

import Foundation

final class Singleton {

    static let shared = Singleton()
    
    var NackGroundColors: String? = "#4BC751"
    private init(){}
    
}
