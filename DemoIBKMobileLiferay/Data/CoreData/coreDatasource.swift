//
//  coreDatasource.swift
//  DemoIBKMobileLiferay
//
//  Created by Everis on 7/03/19.
//  Copyright © 2019 JJgarcia. All rights reserved.
//


import UIKit
import Foundation
import CoreData

class CoreDataUtils{
    
    static var sharedInstance = CoreDataUtils()
    
    func createData(userData:User){
        print("CreateCoreData", userData)
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        //Now let’s create an entity and new user records.
        let userEntity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
        let userCoreData = NSManagedObject(entity: userEntity, insertInto: managedContext)
        print("createData CoreData --> Write User to core Data")
        let userName : String? = userData.userName ?? ""
        let greeting : String? = userData.greeting ?? ""
        let emailAddress : String? = userData.emailAddress ?? ""
        let lastName : String? = userData.lastName ?? ""
        let firstName : String? = userData.firstName ?? ""
        let lastLoginDate : Int? = userData.lastLoginDate ?? 0
        let jobTitle : String? = userData.jobTitle ?? ""
        let createDate : Int? = userData.createDate ??  0
        let lastLoginIP : String? = userData.lastLoginIP ??  ""
        
        userCoreData.setValue(userName, forKey: "userName")
        userCoreData.setValue(greeting, forKey: "greeting")
        userCoreData.setValue(emailAddress, forKey: "emailAddress")
        userCoreData.setValue(lastName, forKey: "lastName")
        userCoreData.setValue(firstName, forKey: "firstName")
        userCoreData.setValue(lastLoginDate, forKey: "lastLoginDate")
        userCoreData.setValue(jobTitle, forKey: "jobTitle")
        userCoreData.setValue(createDate, forKey: "createDate")
        userCoreData.setValue(lastLoginIP, forKey: "lastLoginIP")
        
        
        //Now we have set all the values. The next step is to save them inside the Core Data
        
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    func retrieveAllData(completion: @escaping (_ usuario:User?, _ error:Error?) -> Void) {
        var usuario : User?
        var error : Error?
        
        print (" CoreData retrieveData ")
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        //        fetchRequest.fetchLimit = 1
        //        fetchRequest.predicate = NSPredicate(format: "username = %@", "Ankur")
        //        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "email", ascending: false)]
        //
        do {
            let result = try managedContext.fetch(fetchRequest)
            var userFromCore :User
            for data in result as! [NSManagedObject] {
                let userName : String = (data.value(forKey: "userName") as! String)
                let greeting : String = (data.value(forKey: "greeting") as! String)
                let emailAddress : String = (data.value(forKey: "emailAddress") as! String)
                let lastName : String = (data.value(forKey: "lastName") as! String)
                let firstName : String = (data.value(forKey: "firstName") as! String)
                let lastLoginDate : Int = (data.value(forKey: "lastLoginDate") as! Int)
                let jobTitle : String = (data.value(forKey: "jobTitle") as! String)
                let createDate : Int = (data.value(forKey: "createDate") as! Int)
                let lastLoginIP : String = (data.value(forKey: "lastLoginIP") as! String)
                
                //let loginType : String = (data.value(forKey: "tiporegistro") as! String)
                userFromCore = User(userName: userName, greeting: greeting, emailAddress: emailAddress, lastName: lastName, firstName: firstName, lastLoginDate: lastLoginDate, jobTitle: jobTitle, createDate: createDate, lastLoginIP: lastLoginIP )
                usuario = userFromCore
            }
            
            completion(usuario,nil)
            print(" coredata persona", usuario as Any)
        } catch {
            
            completion(nil, (error as! Error))
            
            print("Failed")
        }
    }
    
    //    func searchData(userId:String) {
    //        print (" CoreData searchData ")
    //        //As we know that container is set up in the AppDelegates so we need to refer that container.
    //        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
    //
    //        //We need to create a context from this container
    //        let managedContext = appDelegate.persistentContainer.viewContext
    //
    //        //Prepare the request of type NSFetchRequest  for the entity
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
    //
    //        //        fetchRequest.fetchLimit = 1
    //        fetchRequest.predicate = NSPredicate(format: "id = %@", userId)
    //        //        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "email", ascending: false)]
    //        //
    //        do {
    //            let result = try managedContext.fetch(fetchRequest)
    //            for data in result as! [NSManagedObject] {
    //
    //                print("id: " , data.value(forKey: "id") as! String)
    //                print("fullname: ", data.value(forKey: "fullname") as! String)
    //                print("doctype: ",data.value(forKey: "doctype") as! String)
    //                print("docnumber: ",data.value(forKey: "docnumber") as! String)
    //                print("email: ",data.value(forKey: "email") as! String)
    //                print("idface: ",data.value(forKey: "idfacebook") as! String)
    //                print("tokenface: ",data.value(forKey: "tokenfacebook") as! String)
    //                print("phonenumber: ",data.value(forKey: "phonenumber") as! String)
    //
    //            }
    //
    //        } catch {
    //
    //            print("Failed")
    //        }
    //    }
    //
    //    func updateData(userData:User){
    //        print (" CoreData updateData ")
    //        //As we know that container is set up in the AppDelegates so we need to refer that container.
    //        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
    //
    //        //We need to create a context from this container
    //        let managedContext = appDelegate.persistentContainer.viewContext
    //
    //        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Users")
    //        fetchRequest.predicate = NSPredicate(format: "id = %@", userData.id!)
    //        do
    //        {
    //            let test = try managedContext.fetch(fetchRequest)
    //            if test.isEmpty{
    //                print ("no se encontro ID ", userData.id)
    //                createData(userData: userData)
    //            }
    //            else{
    //                print ("si se encontro ID ", userData.id)
    //                let objectUpdate = test[0] as! NSManagedObject
    //
    //                if !(userData.idFacebook?.isEmpty)!{
    //                    objectUpdate.setValue(userData.idFacebook , forKey: "idfacebook")
    //                }
    //
    //                if !(userData.tokenFacebook?.isEmpty)!{
    //                    objectUpdate.setValue(userData.tokenFacebook, forKey: "tokenfacebook")
    //                }
    //
    //                do{
    //                    try managedContext.save()
    //                }
    //                catch
    //                {
    //                    print(error)
    //                }
    //            }
    //
    //        }
    //        catch
    //        {
    //            print(error)
    //        }
    //
    //    }
    
    func deleteAllData(){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        //fetchRequest.predicate = NSPredicate(format: "username = %@", "Ankur3")
        
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            for data in test as! [NSManagedObject] {
                managedContext.delete(data)
            }
            
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
            
        }
        catch
        {
            print(error)
        }
    }
    
    
}

